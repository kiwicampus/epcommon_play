package com.kiwicampus.ep.play.exceptions;

import com.kiwicampus.ep.java.exceptions.KiwiException;

/**
 * Used to signal errors on external APIs and services
 */
@SuppressWarnings("unused")
public class ExternalServiceException extends KiwiException {
    public ExternalServiceException() {
        super("Error calling an external service");
    }

    public ExternalServiceException(String message) {
        super(message);
    }

    public ExternalServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
