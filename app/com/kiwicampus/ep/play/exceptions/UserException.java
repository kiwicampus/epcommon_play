package com.kiwicampus.ep.play.exceptions;

import com.kiwicampus.ep.java.exceptions.KiwiException;

/**
 * Used to signal problems caused by the user. Maybe data provided is not right,
 * or he/she didn't follow the right procedure.
 */
@SuppressWarnings("unused")
public class UserException extends KiwiException {
    public UserException() {
        super("User error");
    }

    public UserException(String message) {
        super(message);
    }
}
