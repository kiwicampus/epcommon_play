package com.kiwicampus.ep.play.exceptions;

import com.kiwicampus.ep.java.exceptions.KiwiException;

/**
 * This exception should be used to signal a severe internal exception. Sometimes, it is possible to
 * check that there is a problem but the code cannot fix it. This exception is perfect
 * for thos escenarios.
 */
@SuppressWarnings("unused")
public class SevereKiwiException extends KiwiException {
    private static final long serialVersionUID = -3918357119539455018L;

    public SevereKiwiException(String s) {
        super(s);
    }

    public SevereKiwiException(String message, Throwable cause) {
        super(message, cause);
    }
}
