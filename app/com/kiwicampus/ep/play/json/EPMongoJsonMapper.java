package com.kiwicampus.ep.play.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.kiwicampus.ep.java.json.EPJsonMapper;
import com.kiwicampus.ep.java.util.Pair;
import org.bson.types.ObjectId;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Same functionality offered before by EPJson, only now it can be configured depending if
 * id fields should be transformed or not to _id
 * <p>
 * This is particularly useful for Mongo conversition
 * <p>
 * Created by jasonoviedo on 3/22/17.
 */
@SuppressWarnings("WeakerAccess")
public class EPMongoJsonMapper extends EPJsonMapper {

    @Override
    protected Pair<String, Object> applyCustomMapping(Pair<String, Object> keyValuePair) {
        String fieldName = keyValuePair.getKey();
        Object value = keyValuePair.getValue();
        if ("id".equals(fieldName) || fieldName.endsWith(".id")) {
            fieldName = fieldName.replace("id", "_id");
            value = object("$oid", value.toString());
        }

        return new Pair<>(fieldName, value);
    }
}
