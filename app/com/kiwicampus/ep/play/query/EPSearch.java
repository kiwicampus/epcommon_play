package com.kiwicampus.ep.play.query;

import com.kiwicampus.ep.java.EPIdObject;
import com.kiwicampus.ep.java.json.EPJson;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created with {@link EPCrudService#search(String)}, this class contains
 * data and methods to ease performing a search
 *
 * @param <T>
 */
public class EPSearch<T extends EPIdObject> {
    private final String search;
    private final EPCrudService<T> crudService;

    /**
     * This constructor is NOT intended for use in regular code.
     *
     * @param crudService Crud service associated to this ep.com.kiwicampus.ep.play.query
     * @param search      Initial filter
     */
    EPSearch(EPCrudService<T> crudService, String search) {
        this.crudService = crudService;
        this.search = search.replace(" ", "|");
    }

    public EPQuery<T> inFields(String... fields) {
        Object[] calculatedFields = Arrays.stream(fields)
                .map(field -> EPJson.object(field, EPJson.object("$regex", search, "$options", "i")))
                .collect(Collectors.toList())
                .toArray();
        return new EPQuery<>(crudService, "$or", calculatedFields);
    }
}
