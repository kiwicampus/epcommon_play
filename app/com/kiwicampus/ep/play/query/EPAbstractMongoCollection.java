package com.kiwicampus.ep.play.query;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.kiwicampus.ep.java.exceptions.KiwiException;
import com.kiwicampus.ep.java.json.EPJson;
import com.kiwicampus.ep.play.exceptions.UserException;
import com.kiwicampus.ep.play.models.EPTransaction;
import com.kiwicampus.ep.play.models.EPTransactionEnum;
import com.kiwicampus.ep.play.util.EPDebuggable;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;
import org.jetbrains.annotations.NotNull;
import org.jongo.MongoCollection;
import uk.co.panaxiom.playjongo.PlayJongo;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Base class for the mongo API
 *
 * @param <T>
 * @param <Q>
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class EPAbstractMongoCollection<T, Q extends EPAbstractQuery<T, Q>> extends EPDebuggable {
    protected String collectionName;
    protected Class<T> clazz;

    /**
     * Function to execute before running a create/update request to the db
     */
    private Consumer<EPTransaction<T>> beforeSaveAction;

    /**
     * Function to execute after running a create/update request to the db
     */
    private Consumer<EPTransaction<T>> afterSaveAction;

    public EPAbstractMongoCollection(String collectionName, Class<T> clazz) {
        this.collectionName = collectionName;
        this.clazz = clazz;
        beforeSaveAction = null;
        afterSaveAction = null;
    }

    /**
     * Set an action to execute before saving to the db. If there is an action previously configured, it throws an error
     */
    public void setBeforeSaveAction(Consumer<EPTransaction<T>> beforeSaveAction) {
        if(this.beforeSaveAction == null)
            this.beforeSaveAction = beforeSaveAction;
        else
            throw new RuntimeException("Cannot set more than one beforeAction on an EPCrud");
    }

    /**
     * Set an action to execute after saving to the db. If there is an action previously configured, it throws an error
     */
    public void setAfterSaveAction(Consumer<EPTransaction<T>> afterSaveAction) {
        if(this.afterSaveAction == null)
            this.afterSaveAction = afterSaveAction;
        else
            throw new RuntimeException("Cannot set more than one afterAction on an EPCrud");
    }

    /**
     * Checks if there is any beforeAction configured, and if there is, executes it
     *
     * @param operation The save action we are trying to run right now
     * @param object The provided object before running the transaction
     */
    protected void applyBeforeAction(EPTransactionEnum operation, T object) {
        if(beforeSaveAction != null) {
            EPTransaction<T> transaction = initTransaction(operation);
            transaction.setOriginal(object);
            beforeSaveAction.accept(transaction);
        }
    }

    /**
     * Checks if there is any afterAction configured, and if there is, executes it
     *
     * @param operation The save action we just ran
     * @param object The updated object
     */
    protected void applyAfterAction(EPTransactionEnum operation, T object) {
        if(afterSaveAction != null) {
            EPTransaction<T> transaction = initTransaction(operation);
            transaction.setOriginal(object);
            transaction.setChanged(object);
            afterSaveAction.accept(transaction);
        }

    }

    private EPTransaction<T> initTransaction(EPTransactionEnum op) {
        EPTransaction<T> transaction = EPTransaction.withOperation(op);
        transaction.setCollectionName(collectionName);
        transaction.setEntityName(clazz.getSimpleName());
        return transaction;
    }

    public String getCollectionName() {
        return collectionName;
    }

    /**
     * List items, limited to 200 items. MongoDB does not guarantee ordering
     *
     * @return Top 200 items in the collection.
     */
    public Iterable<T> list() {
        return query().limit(200).cursor();
    }

    public int hardDelete(Object... query) {
        return query(query).hardDelete();
    }

    /**
     * Only update the fields provided by the updateFields. This method should be more efficient than saving the
     * entire object
     *
     * @param id           Document id
     * @param updateFields List of key, value pairs, in the form field1, value1, .... fieldN, valueN. Fields will be set to
     *                     its corresponding values
     * @throws KiwiException If Document is not found by id
     */
    public void updateFields(String id, Object... updateFields) {
        debugStart("udapteFields, id: " + id);
        ObjectNode update = EPJson.object(updateFields);
        WriteResult res = collection().update(toObjectId(id)).with(EPJson.string("$set", update));
        debug("udapteFields", EPJson.toPrettyPrint(update), "");
        if (res.getN() == 0)
            throw new UserException(clazz.getSimpleName() + " could not be found by id");
    }

    @NotNull
    public static ObjectId toObjectId(String id) {
        try {
            return new ObjectId(id);
        } catch (Exception ignored) {
            throw new UserException("Invalid ObjectId: " + id);
        }
    }

    /**
     * This method allows to get a reference to the encapsulated Jongo collection. You can use it
     * to get such a reference and make use of all the Mongo querying capabilities.
     * Of course, you loose automatic mapping to represented class
     *
     * @return {@link MongoCollection Refrence} to encapsulated Jongo collection
     */
    public MongoCollection collection() {
        return collections.computeIfAbsent(collectionName, key -> PlayJongo.getCollection(collectionName));
    }

    /**
     * Saves the object to te DB. If the creation/update fails, an exception is thrown.
     *
     * @param object Object to be saved
     * @return The same object received as param. The id might have been updated if the operation resulted
     * in a creation.
     * @throws KiwiException If the operation fails
     */
    @Nonnull
    public T save(T object) {
        try {
            applyBeforeAction(EPTransactionEnum.Save, object);
            debugStart("Save: " + collectionName);
            WriteResult res = collection().save(object);
            debug("save", "", "");
            if (res.getUpsertedId() != null) {
                return object;
            }
            throw new KiwiException("Entity " + clazz.getSimpleName() + " cannot be created :(");
        } finally {
            applyAfterAction(EPTransactionEnum.Save, object);
        }
    }

    /**
     * Shorthand for query(filter...).findOne()
     *
     * @param filter Query
     * @return Object found. It can be null
     */
    @Nullable
    public T findOne(Object... filter) {
        return query(filter).findOne();
    }

    @Nullable
    public T find1(Object... filter) {
        return query(filter).findOne();
    }

    public Optional<T> tryFindOne(Object... filter) {
        return query(filter).tryFindOne();
    }


    /**
     * Sets the object defined by id as deleted
     *
     * @param id Id to look for
     * @throws KiwiException If document not found by id
     */
    public void delete(String id) {
        int deleted = query("id", id)
                .updateFields("deleted", true);
        if (deleted <= 0)
            throw new UserException(clazz.getSimpleName() + " could not be deleted by id");
    }

    /**
     * Creates a {@link EPQuery query} to be query against this crud's collection
     * <br><br>
     * EX: <br>
     * {@code query("name", "John").project("name", "lastName").sort("age", 1).limit(10).query() }
     *
     * @param query List of parameters to filter by in the form key0, value0, ..., keyn, valueN
     *              where keyi is a field to filter by and valuei is the value to match
     * @return Query to be query. The search process can be refined by it's methods
     */
    public abstract Q query(Object... query);

    public Class<T> getClazz() {
        return clazz;
    }

    private static Map<String, MongoCollection> collections = new HashMap<>();

    @Override
    protected void debug(String method, String query, String projection) {

    }
}
