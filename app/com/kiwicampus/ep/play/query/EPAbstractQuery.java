package com.kiwicampus.ep.play.query;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.kiwicampus.ep.java.json.EPJson;
import com.kiwicampus.ep.play.exceptions.UserException;
import com.kiwicampus.ep.play.util.EPDebuggable;
import com.mongodb.WriteResult;
import org.apache.commons.lang3.ArrayUtils;
import org.jetbrains.annotations.NotNull;
import org.jongo.Find;
import org.jongo.FindOne;
import org.jongo.MongoCursor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.kiwicampus.ep.play.util.EPQueryDebuggers.EPQueryDebugger;


/**
 * Base class for EP queries
 *
 * @param <ELEM_TYPE> Type of the elements returned by this query object
 * @param <QUERY>     Materialized query class (used as return type for method chaining)
 */
@SuppressWarnings({"WeakerAccess", "UnusedReturnValue", "unused"})
public abstract class EPAbstractQuery<ELEM_TYPE, QUERY extends EPAbstractQuery<ELEM_TYPE, QUERY>> extends EPDebuggable {
    private Integer limit;
    private Integer offset;
    private Object[] query;
    private String[] includeFields = ArrayUtils.EMPTY_STRING_ARRAY;
    private String[] excludeFields = ArrayUtils.EMPTY_STRING_ARRAY;
    private Object[] sortFields = ArrayUtils.EMPTY_OBJECT_ARRAY;
    private EPAbstractMongoCollection<ELEM_TYPE, ?> mongoCollection;
    private boolean excludeDeleted = true;

    //#######################################################
    // QUERY METHODS
    //#######################################################

    /**
     * This constructor is NOT intended for use in regular code.
     *
     * @param crudService Crud service associated to this ep.com.kiwicampus.ep.play.query
     * @param query       Initial filter
     */
    public EPAbstractQuery(EPAbstractMongoCollection<ELEM_TYPE, ?> crudService, Object... query) {
        this.query = query;
        this.mongoCollection = crudService;
    }

    /**
     * Add a new set with conditions to the ep.com.kiwicampus.ep.play.query. The conditions added here will be appended to the initial ep.com.kiwicampus.ep.play.query
     * as if you had created a this instance with them all at once.
     *
     * <b>Example:</b>
     * <br>
     * {@code mongoCollection.ep.com.kiwicampus.ep.play.query("k1", v1).addFilter("k2", v2) == mongoCollection.ep.com.kiwicampus.ep.play.query("k1", v1, "k2", v2)}
     *
     * @param filter List of key, value pairs with the filter
     * @return This ep.com.kiwicampus.ep.play.query so calls can be chained
     */
    @Nonnull
    public QUERY addFilter(Object... filter) {
        List<Object> queryList = new ArrayList<>();
        queryList.addAll(Arrays.asList(query));
        queryList.addAll(Arrays.asList(filter));
        query = queryList.toArray();
        return getThis();
    }

    @NotNull
    protected abstract QUERY getThis();

    /**
     * Sets the maximum number with documents to fetch object database
     * EX: {@code ep.com.kiwicampus.ep.play.query("name", "John").limit(10)}
     *
     * @param limit Maximum amount with documents to fetch object database
     * @return This ep.com.kiwicampus.ep.play.query so method calls can be chained
     */
    @Nonnull
    public QUERY limit(int limit) {
        this.limit = limit;
        return getThis();
    }

    /**
     * Skip the first <i>offset</i> items with the results
     *
     * @param offset Number with items to skip
     * @return This object so calls can be chained.
     */
    @Nonnull
    public QUERY offset(Integer offset) {
        this.offset = offset;
        return getThis();
    }

    /**
     * Sets the fields that will be included in the object fetched from the database.
     * If both project() and exclude() are set, only project() will be taken into account
     * EX: {@code ep.com.kiwicampus.ep.play.query("name", "John").project("name", "lastName") }
     *
     * @param fields List with field names to be fetched object data base
     * @return This ep.com.kiwicampus.ep.play.query so method calls can be chained
     */
    @Nonnull
    public QUERY project(String... fields) {
        includeFields = fields;
        return getThis();
    }

    /**
     * Sets the fields that will be excluded in the object fetched from the database.
     * If both project() and exclude() are set, only project() will be taken into account
     * EX: {@code ep.com.kiwicampus.ep.play.query("name", "John").exclude("name", "lastName") }
     *
     * @param fields List with field names to be fetched object data base
     * @return This ep.com.kiwicampus.ep.play.query so method calls can be chained
     */
    @Nonnull
    public QUERY exclude(String... fields) {
        excludeFields = fields;
        return getThis();
    }

    /**
     * Sets the sort order for this ep.com.kiwicampus.ep.play.query
     * EX: {@code ep.com.kiwicampus.ep.play.query("name", "John").sort("lastName", -1)}
     *
     * @param sortFields List with parameter to sort by, in the form key0, sortOrder0, ..., keyn, sortOrderN
     *                   where keyi is a field to sort by, and sortOrderi is either 1 or -1. Sort order 1 will sort
     *                   ascending, while -1 descending.
     * @return This ep.com.kiwicampus.ep.play.query so method calls can be chained
     */
    @Nonnull
    public QUERY sort(Object... sortFields) {
        this.sortFields = sortFields;
        return getThis();
    }

    /**
     * By default, all queries ignore the deleted documents in the database. use this method
     * to override
     *
     * @param include Whether or not to include deleted documents
     * @return This ep.com.kiwicampus.ep.play.query so method calls can be chained
     */
    @Nonnull
    public QUERY includeDeleted(boolean include) {
        excludeDeleted = !include;
        return getThis();
    }

    public QUERY debug(EPQueryDebugger.DebugLevel debugLevel) {
        this.debugger = EPQueryDebugger.getFor(debugLevel);
        return getThis();
    }

    public QUERY debug(EPQueryDebugger.DebugLevel debugLevel, long queryDurationWarning) {
        this.debugger = EPQueryDebugger.getFor(debugLevel, queryDurationWarning);
        return getThis();
    }

    //#######################################################
    // DIFFERENT FLAVORS OF RESULT
    //#######################################################

    /**
     * Runs the current ep.com.kiwicampus.ep.play.query and gets the results as a Mongo cursor
     *
     * @return Mongo cursor with documents found. The returned instance should be closed after use
     */
    @Nonnull
    public MongoCursor<ELEM_TYPE> cursor() {
        String preparedQuery = prepareQuery();
        Find findQuery = mongoCollection.collection().find(preparedQuery);
        if (limit != null)
            findQuery.limit(limit);
        if (offset != null)
            findQuery.skip(offset);
        if (sortFields != null)
            findQuery.sort(EPJson.string(sortFields));
        String projection = setProjection(findQuery);
        debugStart(preparedQuery);
        MongoCursor<ELEM_TYPE> as = findQuery.as(mongoCollection.getClazz());
        debug("cursor", preparedQuery, projection);
        return as;
    }

    /**
     * Runs the current ep.com.kiwicampus.ep.play.query and gets the results as a List
     *
     * @return List with the results
     */
    @Nonnull
    public List<ELEM_TYPE> list() {
        MongoCursor<ELEM_TYPE> cursor = cursor();
        List<ELEM_TYPE> res = StreamSupport.stream(cursor.spliterator(), false).collect(Collectors.toList());
        try {
            cursor.close();
        } catch (IOException ignored) {
            // Ignore the exception, if we already got the results, it does not matter
        }

        return res;
    }

    /**
     * Runs the current query and gets the results as a Map, where the keys are obtained by applying the
     * supplied keyMapper to each elem and values are the returned objects. It is the caller responsibility
     * to ensure the keys are unique. Otherwise, some values might be lost
     *
     * @param keyMapper Function to map element to key
     * @return Map containging the mapped results
     */
    @Nonnull
    public Map<String, ELEM_TYPE> map(Function<ELEM_TYPE, String> keyMapper) {
        Map<String, ELEM_TYPE> res = new HashMap<>();
        try (MongoCursor<ELEM_TYPE> cursor = cursor()) {
            cursor.forEach(elem -> res.put(keyMapper.apply(elem), elem));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return res;

    }

    /**
     * Runs the current ep.com.kiwicampus.ep.play.query and gets the result as a Stream, so functional methods can be applied to it
     *
     * @return Stream with the results
     */
    @Nonnull
    public Stream<ELEM_TYPE> stream() {
        return StreamSupport.stream(cursor().spliterator(), false);
    }

    /**
     * Runs the current ep.com.kiwicampus.ep.play.query by counting the items found
     *
     * @return Number with items found
     */
    public long count() {
        String q = prepareQuery();
        debugStart(q);
        long count = mongoCollection.collection().count(q);
        debug("count", q, "{}");
        return count;
    }

    /**
     * Returns one result for the current query. If no result is found, an {@link UserException exception}  is raised.
     * This way this method can be assumed to always be non null.
     *
     * @return The found object
     * @throws UserException if no results found
     */
    @Nonnull
    public ELEM_TYPE getOne() {
        ELEM_TYPE res = findOne();
        if (res == null)
            throw new UserException(getClass().getSimpleName() + " cannot be found with the given the query");
        return res;
    }

    /**
     * Runs the current ep.com.kiwicampus.ep.play.query, only gets one element if any
     *
     * @return The element found if any. Can bu null
     */
    @Nullable
    public ELEM_TYPE findOne() {
        String prepareQuery = prepareQuery();
        FindOne findQuery = mongoCollection.collection().findOne(prepareQuery);

        if (sortFields != null)
            findQuery.orderBy(EPJson.string(sortFields));

        String projection = setProjection(findQuery);
        debugStart(prepareQuery);
        ELEM_TYPE as = findQuery.as(mongoCollection.getClazz());
        debug("findOne", prepareQuery, projection);
        return as;
    }

    public Optional<ELEM_TYPE> tryFindOne() {
        return Optional.ofNullable(findOne());
    }

    //#######################################################
    // UPDATE/DELETE
    //#######################################################

    public int hardDelete() {
        String prepareQuery = prepareQuery();
        String projection = "";

        debugStart(prepareQuery);
        int n = mongoCollection
                .collection()
                .remove(prepareQuery)
                .getN();
        debug("hardDelete", prepareQuery, projection);
        return n;
    }

    public int delete() {
        String prepareQuery = prepareQuery();
        debugStart(prepareQuery);
        WriteResult res = mongoCollection
                .collection()
                .update(prepareQuery)
                .multi()
                .with(EPJson.string("$set", EPJson.object("deleted", true)));

        debug("delete", prepareQuery, "");
        return res.getN();
    }

    public int updateFields(Object... updateFields) {
        String prepareQuery = prepareQuery();
        debugStart(prepareQuery);
        int affectedDocumentCount = mongoCollection
                .collection()
                .update(prepareQuery)
                .multi()
                .with(EPJson.string("$set", EPJson.object(updateFields)))
                .getN();
        debug("updateFields", prepareQuery, "");
        return affectedDocumentCount;
    }

    //#######################################################
    // INTERNAL LOGIC
    //#######################################################

    protected String prepareQuery() {
        ObjectNode preparedQuery = EPMongo.object(query);
        if (excludeDeleted)
            preparedQuery.set("deleted", EPMongo.ne(true));
        return preparedQuery.toString();
    }

    protected <F> String setProjection(F findQuery) {


        List<Object> projectionArray = includeFields.length > 0 ?
                getProjectionArrayFor(includeFields, true)
                : getProjectionArrayFor(excludeFields, false);


        String projection = EPJson.string(projectionArray.toArray());

        if (!projectionArray.isEmpty()) {
            if (findQuery instanceof Find)
                ((Find) findQuery).projection(projection);
            else if (findQuery instanceof FindOne)
                ((FindOne) findQuery).projection(projection);
        }
        return projection;
    }

    protected List<Object> getProjectionArrayFor(String[] projectedFields, boolean include) {
        List<Object> pfs = new ArrayList<>();
        int e = include ? 1 : 0;
        for (String f : projectedFields) {
            pfs.add(f);
            pfs.add(e);
        }
        return pfs;
    }

    /**
     * Will only print debug info if debug_level is info or warning
     *
     * @param method     method name
     * @param query      Query constructed
     * @param projection Projection
     */
    @Override
    protected void debug(String method, String query, String projection) {
        EPQueryDebugger debugger = getDebugger();

        debugger.printTime(mongoCollection.getClazz().getSimpleName(), query);

        debugger.info("Debugging EPQuery." + method + "() for clazz: " + mongoCollection.getClazz().getSimpleName());
        debugger.info("query = " + query);
        debugger.info("projection = " + projection);
        debugger.info("sort = " + EPJson.string(sortFields));
        debugger.info("offset = " + offset);
        debugger.info("limit = " + limit);

    }

}
