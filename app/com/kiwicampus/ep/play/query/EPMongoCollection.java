package com.kiwicampus.ep.play.query;

public class EPMongoCollection<T> extends EPAbstractMongoCollection<T, EPBaseQuery<T>> {

    public EPMongoCollection(String collectionName, Class<T> clazz) {
        super(collectionName, clazz);
    }

    @Override
    public EPBaseQuery<T> query(Object... query) {
        return new EPBaseQuery<>(this, query);
    }
}
