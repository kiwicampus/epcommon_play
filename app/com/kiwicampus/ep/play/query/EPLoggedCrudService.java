package com.kiwicampus.ep.play.query;

import com.kiwicampus.ep.java.EPIdObject;
import com.kiwicampus.ep.play.loggers.EPCrudLogger;
import com.kiwicampus.ep.play.models.EPTransactionEnum;

/**
 * This will be a special crud that should be used when we want to keep track of the changes to a collection, given a
 * configured {@link EPCrudLogger}
 *
 * @param <T> The type of object the crud will handle
 */
public class EPLoggedCrudService<T extends EPIdObject> extends EPCrudService<T>{

    public EPLoggedCrudService(String collectionName, Class<T> clazz, EPCrudLogger logger) {
        super(collectionName, clazz);
        setAfterSaveAction(logger.asyncLogger());
    }

    /**
     * Since we need the updated object to apply an afterSaveAction, we had to query it in the db before running the hook
     *
     * @param id           Document id
     * @param updateFields List of key, value pairs, in the form field1, value1, .... fieldN, valueN. Fields will be set to
     *                     its corresponding values
     */
    @Override
    @Deprecated
    public void updateFields(String id, Object... updateFields) {
        super.updateFields(id, updateFields);
        T updated = findById(id);
        applyAfterAction(EPTransactionEnum.Update, updated);
    }
}
