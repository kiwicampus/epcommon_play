package com.kiwicampus.ep.play.query;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings("WeakerAccess")
public class EPBaseQuery<T> extends EPAbstractQuery<T, EPBaseQuery<T>>{

    /**
     * This constructor is NOT intended for use in regular code.
     *
     * @param crudService Crud service associated to this ep.com.kiwicampus.ep.play.query
     * @param query       Initial filter
     */
    public EPBaseQuery(EPMongoCollection<T> crudService, Object... query) {
        super(crudService, query);
    }

    @NotNull
    @Override
    protected EPBaseQuery<T> getThis() {
        return this;
    }
}
