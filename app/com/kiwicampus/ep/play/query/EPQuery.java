package com.kiwicampus.ep.play.query;

import com.kiwicampus.ep.java.EPIdObject;
import com.kiwicampus.ep.play.util.EPQueryDebuggers;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * Class that encapsulates the info with a ep.com.kiwicampus.ep.play.query. It's somewhat easier to use and read queries that use this class
 * over native Jongo ones. Still, the intent with this class is not to encapsulate Jongo, but to offer a simple API
 * for common cases. If more powerful queries are needed, you should fall back to native Jongo.
 */
@SuppressWarnings({"WeakerAccess", "UnusedReturnValue", "unused"})
public class EPQuery<T extends EPIdObject> extends EPAbstractQuery<T, EPQuery<T>> {

    //#######################################################
    // QUERY METHODS
    //#######################################################

    /**
     * This constructor is NOT intended for use in regular code.
     *
     * @param crudService Crud service associated to this ep.com.kiwicampus.ep.play.query
     * @param query       Initial filter
     */
    EPQuery(EPCrudService<T> crudService, Object... query) {
        super(crudService, query);
    }

    //#######################################################
    // DIFFERENT FLAVORS OF RESULT
    //#######################################################

    /**
     * Runs the current query and gets the results as a Map, where the keys are the objects' ids and
     * values are the returned objects
     *
     * @return Map with the results
     */
    @Nonnull
    public Map<String, T> map() {
        return super.map(EPIdObject::getId);
    }

    @NotNull
    @Override
    protected EPQuery<T> getThis() {
        return this;
    }
}