package com.kiwicampus.ep.play.query;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.kiwicampus.ep.java.json.EPJson;
import com.kiwicampus.ep.java.json.EPJsonMapper;
import com.kiwicampus.ep.play.json.EPMongoJsonMapper;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Some usual Mongo queries in a simpler way
 * Created by jasonoviedo on 11/29/16.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class EPMongo {


    private static final EPJsonMapper mongoMapper = new EPMongoJsonMapper();

    /**
     * Just as {@link EPJson#object(Object...)} but using a custom mapper
     * that forces all <code>id</code> fields into <code>_id</code> as {@link ObjectId}
     *
     * @param params Params
     * @return Json
     */
    public static ObjectNode object(Object... params) {
        return mongoMapper.object(params);
    }

    /**
     * Used to match a value with a Regex
     */
    public static ObjectNode regex(String regex) {
        return object("$regex", regex);
    }

    /**
     * Short for Mongo {$ne: value}
     *
     * @param value Value
     * @return Json representation
     */
    public static ObjectNode ne(Object value) {
        return object("$ne", value);
    }

    /**
     * Short for Mongo {$lt: value}
     *
     * @param value Value
     * @return Json representation
     */
    public static ObjectNode lt(Object value) {
        return object("$lt", value);
    }

    /**
     * Short for Mongo {$gt: value}
     *
     * @param value Value
     * @return Json representation
     */
    public static ObjectNode gt(Object value) {
        return object("$gt", value);
    }

    /**
     * Short for Mongo {$lte: value}
     *
     * @param value Value
     * @return Json representation
     */
    public static ObjectNode lte(Object value) {
        return object("$lte", value);
    }

    /**
     * Short for Mongo {$gte: value}
     *
     * @param value Value
     * @return Json representation
     */
    public static ObjectNode gte(Object value) {
        return object("$gte", value);
    }

    /**
     * Short for Mongo {$gt: from, $lt: to}
     *
     * @param from From value
     * @param to   To value
     * @return Json representation
     */
    public static ObjectNode between(Long from, Long to) {
        return object("$gt", from, "$lt", to);
    }

    /**
     * Short for Mongo {$gte: from, $lte: to}
     *
     * @param from From value
     * @param to   To value
     * @return Json representation
     */
    public static ObjectNode betweenInclusive(long from, long to) {
        return object("$gte", from, "$lte", to);
    }

    /**
     * {"$in": [id1, id2, .., idn]}
     *
     * @param ids Ids as String
     * @return Json for Mongo ep.com.kiwicampus.ep.play.query
     */
    public static Object inIdList(Iterable<String> ids) {
        List<ObjectId> objectIds =
                StreamSupport.stream(ids.spliterator(), false)
                        .map(ObjectId::new)
                        .collect(Collectors.toList());
        return object("$in", objectIds);
    }

    /**
     * Short for Mongo {$in: params}
     *
     * @param params Params
     * @return Json
     */
    public static ObjectNode in(Object... params) {
        return object("$in", params);
    }

    /**
     * Shor for Mongo {$nin: params}
     */
    public static ObjectNode nin(Object... params) {
        return object("$nin", params);
    }

    public static ObjectNode inList(List<?> params) {
        return object("$in", params);
    }

    public static ObjectNode aInB(String key, List<?> objects) {
        List<?> objects1 = key.equals("id") || key.endsWith(".id") ?
                objects.stream().map(o -> EPAbstractMongoCollection.toObjectId(o.toString())).collect(Collectors.toList())
                : objects;
        return object("$in", objects1);
    }


    //*********************************** AGGREGATE ***********************************//

    /**
     * Short for Mongo {$match: filterDocument} where filterDocument
     * is constructed using {@link #object(Object...) object(filter)}
     *
     * @param filter Filter document
     * @return Json
     */
    public static ObjectNode match(Object... filter) {
        return object("$match", object(filter));
    }

    public static ObjectNode project(Object... projection) {
        return object("$project", object(
                projection
        ));
    }

    /**
     * Short for Mongo {$group: filterDocument} where filterDocument
     * is constructed using {@link #object(Object...) object(filter)}
     *
     * @param filter Filter document
     * @return Json
     */
    public static ObjectNode group(Object... filter) {
        return object("$group", object(filter));
    }
}
