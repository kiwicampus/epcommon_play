package com.kiwicampus.ep.play.query;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.kiwicampus.ep.java.EPIdObject;
import com.kiwicampus.ep.java.exceptions.KiwiException;
import com.kiwicampus.ep.play.exceptions.SevereKiwiException;
import com.kiwicampus.ep.play.exceptions.UserException;
import com.kiwicampus.ep.play.models.EPTransactionEnum;
import com.mongodb.WriteResult;
import org.jongo.FindOne;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jasonoviedo on 6/22/16.
 * <p>
 * This class encapsulates a little with the inner workings with a Mongo (Jongo) collection
 * The idea IS NOT to replace Jongo, but to ease development, by offering some syntax sugar
 * for some common tasks, so you don't have to think to hard for simple, common DB interactions.
 * It also helps make code a little easier to read. But the whole power of the Jongo collection
 * is still there to be used if required.
 *
 * @see EPCrudService#collection()
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class EPCrudService<T extends EPIdObject> extends EPAbstractMongoCollection<T, EPQuery<T>> {


    public EPCrudService(String collectionName, Class<T> clazz) {
        super(collectionName, clazz);
    }

    /**
     * Saves an object to DB as a new document.
     *
     * @param object Object to save
     * @return The object
     */
    @Nonnull
    public T create(T object) {
        try {
            applyBeforeAction(EPTransactionEnum.Create, object);
            object.setId(null);
            WriteResult res = collection().save(object);
            if (res.getUpsertedId() != null) {
                object.setId(res.getUpsertedId().toString());
                return object;
            }
            throw new SevereKiwiException("Entity " + clazz.getSimpleName() + " cannot be created :(");
        } finally {
            applyAfterAction(EPTransactionEnum.Create, object);
        }
    }

    /**
     * Updates an object in DB looking by its id
     *
     * @param id     Id to look for
     * @param object Object to save to DB
     * @return Object saved
     */
    @Nonnull
    public T update(String id, T object) {
        try {
            applyBeforeAction(EPTransactionEnum.Update, object);
            object.setId(id);
            WriteResult res = collection().update(toObjectId(id)).with(object);
            if (res.getN() > 0)
                return object;
            throw new UserException("Entity " + clazz.getSimpleName() + " with id " + id + " cannot be updated :(");
        } finally {
            applyAfterAction(EPTransactionEnum.Update, object);
        }
    }

    /**
     * Saves the object to te DB. This operation performs an upsert depending on whether the object
     * has an id or not. If the creation/update fails, an exception is thrown.
     *
     * @param object Object to be saved
     * @return The same object received as param. The id might have been updated if the operation resulted
     * in a creation.
     * @throws KiwiException If the operation fails
     */
    @Nonnull
    public T save(T object) {
        if (object.getId() != null)
            return update(object.getId(), object);

        return create(object);
    }


    /**
     * Retrieves an object with DB by its id. Method is guaranteed to return
     * an object or fail if it cannot be found
     *
     * @param id Id to look for
     * @return The object.
     * @throws KiwiException if object cannot be found
     */
    @Nonnull
    public T getById(String id) {
        T res = findById(id);
        if (res == null)
            throw new UserException("Entity " + clazz.getSimpleName() + " can't be found by id: " + id);
        return res;
    }

    /**
     * Tries to retrieve an object with DB by its Id
     *
     * @param id Id to look for
     * @return Object found or null
     */
    @Nullable
    public T findById(String id) {
        FindOne one = collection().findOne(toObjectId(id));
        return one.as(clazz);
    }

    /**
     * Updates the object in DB, ONLY if the conditions holds true. The object is located by id
     *
     * @param object    Object to save
     * @param condition Condition to check
     * @return Object if update was performed
     */
    public T updateConditional(T object, Object... condition) {
        try {
            applyBeforeAction(EPTransactionEnum.Update, object);
            checkId(object);
            ArrayList<Object> filter2 = new ArrayList<>();
            filter2.addAll(Arrays.asList("id", object.getId()));
            filter2.addAll(Arrays.asList(condition));
            ObjectNode query = EPMongo.object(filter2.toArray());
            WriteResult res = collection().update(query.toString()).with(object);
            if (res.getN() > 0)
                return object;
            return null;
        } finally {
            applyAfterAction(EPTransactionEnum.Update, object);
        }
    }


    @Override
    public EPQuery<T> query(Object... query) {
        return new EPQuery<>(this, query);
    }

    public EPSearch<T> search(String search) {
        return new EPSearch<>(this, search);
    }

    private void checkId(T object) {
        if (object.getId() == null)
            throw new UserException("ID can't be null");
    }


}
