package com.kiwicampus.ep.play.util;

public abstract class EPDebuggable {
    protected EPQueryDebuggers.EPQueryDebugger debugger = EPQueryDebuggers.EPQueryDebugger.getFor(EPQueryDebuggers.EPQueryDebugger.DEBUG_LEVEL);

    protected void debugStart(String prepareQuery) {
        getDebugger().debugStart(prepareQuery);
    }

    protected abstract void debug(String method, String query, String projection);

    protected EPQueryDebuggers.EPQueryDebugger getDebugger() {
        return debugger;
    }
}
