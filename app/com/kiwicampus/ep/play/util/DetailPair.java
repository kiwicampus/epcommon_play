package com.kiwicampus.ep.play.util;

/**
 * VO to send an array instead of a map
 * Created by jasonoviedo on 1/23/17.
 */
public class DetailPair extends AbstractDetailPair<Double> {

    public static DetailPair of(String name, double value) {
        DetailPair detailPair = new DetailPair();
        detailPair.name = name;
        detailPair.value = value;
        return detailPair;
    }
}
