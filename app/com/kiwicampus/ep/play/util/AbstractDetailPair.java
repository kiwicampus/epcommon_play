package com.kiwicampus.ep.play.util;

/**
 * VO to send an array instead of a map. This helps keep order
 * Created by jasonoviedo on 1/23/17.
 */
public class AbstractDetailPair<T> {

    protected String name;
    protected T value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public static <T> AbstractDetailPair<T> of(String name, T value) {
        AbstractDetailPair<T> detailPair = new AbstractDetailPair<>();
        detailPair.name = name;
        detailPair.value = value;
        return detailPair;
    }
}
