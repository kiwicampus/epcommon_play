package com.kiwicampus.ep.play.util;

public interface EPQueryDebuggers {
    abstract class EPQueryDebugger {

        public enum DebugLevel {
            none,
            error,
            warning,
            info
        }

        public void debugStart(String query) {
            if (debugStart > -1)
                error("debugStart() called twice in a row when calling " + query);
            debugStart = System.currentTimeMillis();
        }

        public void printTime(String collection, String query) {
            if (debugStart == -1)
                error("debugStart() not called at the beginning of the call of " + query);

            long totalTime = System.currentTimeMillis() - debugStart;
            debugStart = -1;

            if (totalTime > queryDurationWarning)
                warning("Slow query: " + totalTime + "ms taken querying collection: " + collection + ", filter: " + query);
        }

        public void info(String s) {

        }

        public void warning(String s) {

        }

        public void error(String s) {
        }


        /**
         * Default time for warnings. Queries that take longer than this will trigger a Warning if
         * #getDebugLevel() is info or warning
         */
        public static long QUERY_DURATION_WARNING = 30L;
        public static DebugLevel DEBUG_LEVEL = DebugLevel.error;

        private long debugStart = -1;
        private Long queryDurationWarning = QUERY_DURATION_WARNING;

        public static EPQueryDebugger getFor(DebugLevel debugLevel) {
            EPQueryDebugger debugger = null;
            switch (debugLevel) {
                case none:
                    debugger = new NoOpDebugger();
                    break;
                case error:
                    debugger = new ErrorDebugger();
                    break;
                case warning:
                    debugger = new WarningDebugger();
                    break;
                case info:
                    debugger = new InfoDebugger();
                    break;
            }
            return debugger;
        }

        public static EPQueryDebugger getFor(DebugLevel debugLevel, long queryDurationWarning) {
            EPQueryDebugger debugger = getFor(debugLevel);
            debugger.queryDurationWarning = queryDurationWarning;
            return debugger;
        }
    }


    class NoOpDebugger extends EPQueryDebugger {
    }

    class ErrorDebugger extends EPQueryDebugger {
        @Override
        public void error(String s) {
            System.out.println("ERROR: " + s);
        }
    }

    class WarningDebugger extends ErrorDebugger {
        @Override
        public void warning(String s) {
            System.out.println("WARNING: " + s);
        }
    }

    class InfoDebugger extends WarningDebugger {
        @Override
        public void info(String s) {
            System.out.println("INFO: " + s);
        }
    }

}


