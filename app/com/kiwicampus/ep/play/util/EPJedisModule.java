package com.kiwicampus.ep.play.util;

import com.google.inject.AbstractModule;
import com.kiwicampus.ep.play.loggers.EPCrudLogger;
import com.kiwicampus.ep.play.loggers.JedisCrudLogger;

/**
 * Sets guice to inject a {@link JedisCrudLogger} by default
 */
public class EPJedisModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(EPCrudLogger.class)
                .to(JedisCrudLogger.class)
                .asEagerSingleton();
    }
}
