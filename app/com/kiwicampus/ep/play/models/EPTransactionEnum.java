package com.kiwicampus.ep.play.models;

public enum EPTransactionEnum {
    Create,
    Update,
    Delete,
    // This might be unnecessary, but the save method in EPAbstractMongoCollection doesn't let us know in advance what
    // type of transaction we are executing
    Save
}
