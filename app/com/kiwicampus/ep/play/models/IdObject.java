package com.kiwicampus.ep.play.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.kiwicampus.ep.java.EPIdObject;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

import java.util.Collections;
import java.util.List;

/**
 * Base model for DB
 * Created by jasonoviedo on 6/22/16.
 */
@JsonInclude(Include.ALWAYS)
public class IdObject implements EPIdObject{

    @MongoId
    @MongoObjectId
    protected String id;

    /**
     * All objects might be marked as deleted, but deleted might not be set for
     * an object, so we use Boolean wrapper instead of the primitive type
     */
    private Boolean deleted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean isDeleted() {
        return deleted == null ? false : deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof IdObject))
            return false;
        IdObject idObj = (IdObject) obj;
        if (id == null || idObj.id == null)
            return false;
        return id.equals(idObj.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public static List<String> getSearchFields() {
        return Collections.singletonList("id");
    }

}
