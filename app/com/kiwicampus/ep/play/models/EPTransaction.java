package com.kiwicampus.ep.play.models;

import javax.annotation.Nullable;

/**
 * This object will contain important data related to a DB transaction made in an EPCrud class
 * @param <T> Type of object handled by the crud
 */
public class EPTransaction<T> {

    /**
     * Type of operation being run
     */
    private EPTransactionEnum operation;

    /**
     * Name of the mongodb collection
     */
    private String collectionName;

    /**
     * Name of the entity handled by the crud
     */
    private String entityName;

    /**
     * Original data for an object before executing a transaction
     */
    @Nullable
    private T original;

    /**
     * Resulting object after the transaction has been executed
     */
    private T changed;

    public EPTransaction() {
        super();
    }

    public static <U> EPTransaction<U> withOperation(EPTransactionEnum operation) {
        EPTransaction<U> transaction = new EPTransaction<>();
        transaction.operation = operation;
        return transaction;
    }

    public EPTransactionEnum getOperation() {
        return operation;
    }

    public void setOperation(EPTransactionEnum operation) {
        this.operation = operation;
    }

    @Nullable
    public T getOriginal() {
        return original;
    }

    public void setOriginal(@Nullable T original) {
        this.original = original;
    }

    public T getChanged() {
        return changed;
    }

    public void setChanged(T changed) {
        this.changed = changed;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }
}
