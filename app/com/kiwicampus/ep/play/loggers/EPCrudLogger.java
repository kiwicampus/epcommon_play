package com.kiwicampus.ep.play.loggers;

import com.kiwicampus.ep.play.models.EPTransaction;

import java.util.function.Consumer;

/**
 * This interface will provide the method an {@link com.kiwicampus.ep.play.query.EPLoggedCrudService} will use to
 * keep track of the changes in the mongodb. In this way, we can have different options, such as Redis, Logback,
 * Elasticsearch, etc, to run the log without having to modify the code too much
 */
public interface EPCrudLogger {
    /**
     * Returns a function that will be added to an EPLoggedCrudService as an after save hook, and that should run
     * the desired "logging" of the db changes
     *
     * @param <T> Type of object that is handled by the crud
     * @return Db change logging function
     */
    <T> Consumer<EPTransaction<T>> asyncLogger();
}
