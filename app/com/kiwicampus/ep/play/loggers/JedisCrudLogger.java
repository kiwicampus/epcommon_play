package com.kiwicampus.ep.play.loggers;

import com.kiwicampus.ep.java.json.EPJson;
import com.kiwicampus.ep.play.models.EPTransaction;
import play.Configuration;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.inject.Inject;
import java.util.function.Consumer;

/**
 * Implements logging of {@link EPTransaction} on a Redis pubsub channel
 */
public class JedisCrudLogger implements EPCrudLogger {

    /**
     * Redis channel to which we will publish the messages. It must be provided in the Play app configuration
     */
    private String redisChannel;

    /**
     * Redis connection client
     */
    private JedisPool pool;

    @Inject
    public JedisCrudLogger(JedisPool pool, Configuration config) {
        this.pool = pool;
        this.redisChannel = config.getString("epcrud.logger.jedis.channel", "epcrud.changeevents");
    }

    @Override
    public <T> Consumer<EPTransaction<T>> asyncLogger() {
        return transaction -> {
            Jedis j = pool.getResource();
            j.publish(redisChannel, EPJson.toString(transaction));
            pool.returnResource(j);
        };
    }
}
