package com.kiwicampus.ep.play.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.kiwicampus.ep.java.json.EPJson;
import com.kiwicampus.ep.play.exceptions.UserException;
import com.kiwicampus.ep.play.models.IdObject;
import com.kiwicampus.ep.play.query.EPAbstractQuery;
import com.kiwicampus.ep.play.query.EPCrudService;
import com.kiwicampus.ep.play.query.EPQuery;
import org.apache.commons.lang3.tuple.Pair;
import play.mvc.Result;

import java.util.*;
import java.util.stream.Collectors;

//import models.Order;

/**
 * Generic controller that allows queries over the controlled entity
 * <p>
 * Created by jasonoviedo on 10/19/16.
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class EPQueryController<T extends IdObject> extends EPSimpleCrudController<T> {

    /**
     * Print debug data if true
     */
    public static boolean debug = false;
    private static final int MAX_LIMIT = 1000;
    public static final int DEFAULT_LIMIT = 30;

    private String defaultQuerySortField;

    protected EPQueryController(EPCrudService<T> crud) {
        super(crud);
    }

    protected void setDefaultQuerySortField(String defaultQuerySortField) {
        this.defaultQuerySortField = defaultQuerySortField;
    }

    protected String getDefaultQuerySortField() {
        return defaultQuerySortField;
    }

    private static class SpecialQueryCase {
        private String queryName;
        private String dbName;
        private String operator;

        SpecialQueryCase(String queryName, String dbName, String operator) {
            this.queryName = queryName;
            this.dbName = dbName;
            this.operator = operator;
        }

        static SpecialQueryCase of(String queryName, String dbName, String operator) {
            return new SpecialQueryCase(queryName, dbName, operator);
        }
    }

    /**
     * Special cases for url ep.com.kiwicampus.ep.play.query parameters
     *
     * <br>status -> {state.status: {$eq: value}}
     * <br>object -> {state.openTimestamp: {$gt: value}}
     * <br>to -> {state.openTimestamp: {$lt: value}}
     */
    private static List<SpecialQueryCase> specialCases = Arrays.asList(
//            SpecialQueryCase.of("zoneId", "zoneId", null),
            SpecialQueryCase.of("status", "state.status", null),
//            SpecialQueryCase.of("kiwerId", "kiwer.id", null),
//            SpecialQueryCase.of("clientId", "clientId", null),
            SpecialQueryCase.of("from", "state.openTimestamp", "$gt"),
            SpecialQueryCase.of("to", "state.openTimestamp", "$lt")
    );

    public Result queryWithDefaults(int limit, String sort) {
        Map<String, String> queryParams = EPJson.castedMap(
                "limit", String.valueOf(limit),
                "sort", sort);
        return ok(genericQuery(queryParams));
    }

    /**
     * "limit", "offset", "sort", "includeFields", "excludeFields", "metaonly"
     *
     * @return Standard query
     */
    public Result query() {
        return ok(genericQuery(Collections.emptyMap()));
    }

    /**
     * Generic ep.com.kiwicampus.ep.play.query. It constructs an {@link EPQuery EPQuery} by applying the parameters
     * in the param Map. The are a couple with {@link EPQueryController#specialCases special common cases}
     * that work as syntactic sugar.
     * <p>
     * For example:
     * {@code /orders?zoneId=some_zone&kiwerId=some_kiwer&status=closed|cancelled}
     * <p> Translates to:
     * {@code {zoneId:some_zone, kiwer.id:some_kiwer, $or:[{state.status:closed}, {state.status:cancelled}]}}
     *
     * @param custom Query parameter
     * @return List with orders found
     */
    protected Iterable<T> genericQuery(Map<String, String> custom) {
        Map<String, String> params = getQueryParams();
        custom.forEach(params::put);
        String sort = params.getOrDefault("sort", defaultQuerySortField);
        int limit = Integer.parseInt(params.getOrDefault("limit", String.valueOf(DEFAULT_LIMIT)));

        if (limit > MAX_LIMIT)
            throw new UserException("Can't return more than " + MAX_LIMIT + " results at a time");

        String search = params.get("q");
        EPCrudService<T> crud = getCrud();
        List<String> fields = getSearchFields(getCrud().getClazz());
        EPAbstractQuery<T, ?> query = search == null
                ? crud.query()
                : crud.search(search).inFields(fields.toArray(new String[fields.size()]));

        //Apply base params
        if (Arrays.asList("asc", "desc").contains(sort))
            query.sort(defaultQuerySortField, "asc".equals(sort) ? 1 : -1);
        else if (sort != null) {
            boolean desc = sort.startsWith("-");
            query.sort(sort.replace("-", ""), desc ? -1 : 1);
        }

        Integer offset = getParamValueAsInt(params, "offset");
        if (offset != null)
            query.offset(offset);

        //Ignore base params object ep.com.kiwicampus.ep.play.query string
        boolean metaonly = Boolean.parseBoolean(params.getOrDefault("metaonly", "false"));

        String[] includeFields = getParamValueAsArray(params, "includeFields");
        String[] excludeFields = getParamValueAsArray(params, "excludeFields");
        Arrays.asList("limit", "offset", "sort", "includeFields", "excludeFields", "metaonly").forEach(params::remove);

        //Apply special cases
        specialCases.stream()
                .filter(sCase -> params.containsKey(sCase.queryName)) // Filter out special cases not present
                .peek(sCase -> {  // Apply each especial case
                    String value = params.get(sCase.queryName);
                    applyFilter(query, sCase.dbName, value, sCase.operator);
                })
                .forEach(sCase -> params.remove(sCase.queryName)) // Remove the special cases so we can continue
        ;

        params.forEach((key, value) -> applyFilter(query, key, value, null));

        long totalCount = query.count();
        query.limit(limit).project(includeFields).exclude(excludeFields);
        long count = query.count();
        response().setHeader("Count", String.valueOf(count));
        response().setHeader("Total-Count", String.valueOf(count));
        return metaonly ? Collections.emptyList() : query.cursor();
    }

    /**
     * Adds a filter to the ep.com.kiwicampus.ep.play.query.
     * <p> For example, parameters: epQuery, "kiwer.name", "John P", "$eq"
     * Filter: {"kiwer.name":{$eq:"John P"}}
     *
     * @param epQuery     EPQuery to add filter to
     * @param filterParam Field name
     * @param value       Value
     * @param operator    operator
     */
    protected void applyFilter(EPAbstractQuery<T, ?> epQuery, String filterParam, String value, String operator) {

        String[] values = value.replace("|", ":").split(":");
        if (values.length > 1) {
            List<ObjectNode> pairs = new ArrayList<>();
            for (String v : values) {
                pairs.add(EPJson.object(filterParam, v));
            }
            epQuery.addFilter("$or", pairs);
        } else
            epQuery.addFilter(filterParam, operator != null ? EPJson.object(operator, safeCast(value)) : safeCast(value));
    }

    /**
     * Tries to parse first to double, the to integer and falls back to String if not able to
     *
     * @param value Value
     * @return Integer, Long, Double or String
     */
    protected Object safeCast(String value) {
        debugPrint("Parsing argument: " + value);
        try {
            debugPrint("Boolean? ");
            if (Arrays.asList("true", "false").contains(value))
                return Boolean.parseBoolean(value);
            debugPrint("Not a boolean");
        } catch (Exception ignored) {
            debugPrint("Not a boolean");
        }
        try {
            debugPrint("Int? ");
            return Integer.parseInt(value);
        } catch (NumberFormatException ignored) {
            debugPrint("Not an Integer");
        }
        try {
            debugPrint("Long? ");

            if (value.matches("\\d+l")) {
                debugPrint("Looks like a long");
                return Long.parseLong(value.replace("l", ""));
            }
        } catch (NumberFormatException ignored) {
            debugPrint("Not a Long");
        }

        try {
            debugPrint("Double? ");
            if (value.matches("\\d+\\.\\d+")) {
                debugPrint("Looks like a double");
                return Double.parseDouble(value);
            }
        } catch (NumberFormatException ignored) {
            debugPrint("Not a Double");
        }
        debugPrint("Looks like a string");
        return value;
    }

    protected void debugPrint(String text) {
        if (debug)
            System.out.println(text);
    }

    protected Map<String, String> getQueryParams() {
        // Play reports queries in the form http;//some.server?a=0&b=1&a=2
        // like Map(a -> [0,2], b -> [1])
        // So we need to get rid with the array part, if there are multiple values
        // for a parameter, we will ignore them
        Map<String, String[]> queryParams = request().queryString();
        // Once we have a much nicer params map: Map(a->0, b->1)
        // we can proceed to invoke our generic ep.com.kiwicampus.ep.play.query method
        return queryParams.entrySet()
                .stream()
                .map(entry -> Pair.of(entry.getKey(), entry.getValue()[0]))
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }

    @SuppressWarnings("SameParameterValue")
    protected Integer getParamValueAsInt(Map<String, String> params, String paramName) {
        Object value = params.get(paramName);
        return value == null ? null : Integer.parseInt(value.toString());
    }

    protected String[] getParamValueAsArray(Map<String, String> params, String paramName) {
        return Arrays.stream(
                params.getOrDefault(paramName, "")
                        .split(","))
                .filter(s -> !s.isEmpty())
                .toArray(String[]::new);
    }

    /**
     * Method should be implemented by sub clases, should return a list of default fields
     * to search in
     *
     * @param clazz The class of the contained model
     * @return List of fields to search into
     */
    protected abstract List<String> getSearchFields(Class clazz);
//    {
//        return Objects.equals(clazz, Order.class)
//                ? Arrays.asList("clien.name", "client.phoneNumber", "kiwer.name", "kiwer.phoneNumber")
//                : IdObject.getSearchFields();
//    }


}