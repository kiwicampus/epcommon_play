package com.kiwicampus.ep.play.controllers;

import com.kiwicampus.ep.play.models.IdObject;
import com.kiwicampus.ep.play.query.EPCrudService;
import play.mvc.Result;

/**
 * Created by jasonoviedo on 12/14/16.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class EPSimpleCrudController<T extends IdObject> extends EPController {

    private static final Object[] EMPTY = new Object[0];

    protected EPCrudService<T> crud;
    private Object[] defaultSort;
    private int defaultLimit;
    private Class<T> entity;
    private Object[] defaultFilter;

    public EPSimpleCrudController(EPCrudService<T> crud) {
        this.crud = crud;
        entity = crud.getClazz();
        defaultSort = EMPTY;
        defaultFilter = EMPTY;
        defaultLimit = 100;
    }


    protected EPCrudService<T> getCrud() {
        return crud;
    }

    protected void setDefaultSort(Object... defaultSort) {
        this.defaultSort = defaultSort;
    }

    protected void setDefaultLimit(int defaultLimit) {
        this.defaultLimit = defaultLimit;
    }

    protected void setDefaultFilter(Object... filter) {
        this.defaultFilter = filter;
    }

    public Result create() {
        return ok(crud.create(bodyAs(entity)));
    }

    public Result create(T object) {
        return ok(crud.create(object));
    }


    public Result list() {
        Iterable<T> list = crud.query(defaultFilter)
                .sort(defaultSort)
                .limit(defaultLimit)
                .cursor();
        return ok(list);
    }

    public Result getById(String id) {
        return ok(crud.getById(id));
    }

    public Result update(String id) {
        return ok(crud.update(id, bodyAs(entity)));
    }

    public Result delete(String id) {
        crud.delete(id);
        return ok();
    }
}
