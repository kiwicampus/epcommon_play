package com.kiwicampus.ep.play.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.kiwicampus.ep.java.json.EPJson;
import com.kiwicampus.ep.play.exceptions.UserException;
import org.jongo.MongoCursor;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http.RequestBody;
import play.mvc.Result;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Base class that adds some standard logic to controllers. All controllers should extend this class
 * <p>
 * Created by jasonoviedo on 6/22/16.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class EPController extends Controller {

    private static final String CONTENT_TYPE = "application/json; charset=utf-8";

    @Nonnull
    protected static <T> T bodyAs(Class<T> clazz) {
        RequestBody body = request().body();
        if (body == null || body.asJson() == null) {
            throw new UserException("This endpoint was expecting a " + clazz.getSimpleName() + "body in json format");
        }
        return Json.fromJson(body.asJson(), clazz);
    }

    protected static Result ok(Object object) {
        Result result = object == null ? ok() : ok(Json.prettyPrint(stripNulls(Json.toJson(object))));
        return result.as(CONTENT_TYPE);
    }

    public static Result error(String message) {
        return error(message, null);
    }

    @SuppressWarnings("SameParameterValue")
    protected static Result error(String message, Integer errorCode) {
        String m = message != null ? message : "Request cannot be processed";
        @SuppressWarnings("MagicNumber")
        int i = errorCode != null ? errorCode : 400;
        return status(i, EPJson.object("message", m, "code", errorCode, "error", m))
                .as(CONTENT_TYPE);
    }

    protected static Result ok(Iterable<?> list) {
        Iterable<?> toWrite = list == null ? new ArrayList() : list;
        Result res = ok(Json.prettyPrint(stripNulls(Json.toJson(toWrite))));
        if (toWrite instanceof MongoCursor)
            try {
                ((MongoCursor) toWrite).close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        return res.as(CONTENT_TYPE);
    }

    protected static Result success(String message) {
        return ok(EPJson.object("message", message))
                .as(CONTENT_TYPE);
    }

    private static JsonNode stripNulls(JsonNode node) {
        Iterator<JsonNode> it = node.iterator();
        while (it.hasNext()) {
            JsonNode child = it.next();
            if (child.isNull())
                it.remove();
            else
                stripNulls(child);
        }

        return node;
    }
}
