Kiwi Campus Common Play framework library

Current version is 1.0

This projects holds common code for most Play framework projects
Among it, there are default definitions for controllers (EPController, EPSimpleCrudController),
implementations for controllers that can be searched (query a list of resources, etc).

This is a work in progress and will evolve considerably.

The main responsible for now is Jason Oviedo