name := """epcommon_play"""

version := "2.10"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

routesGenerator := InjectedRoutesGenerator

resolvers += "jitpack" at "https://jitpack.io"

libraryDependencies ++= Seq(
  javaWs,
  "uk.co.panaxiom" %% "play-jongo" % "1.0.1-jongo1.2",
  "com.typesafe.play" %% "play-json" % "2.5.9",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.8.3",
  "com.fasterxml.jackson.core" % "jackson-annotations" % "2.8.3",
  "com.fasterxml.jackson.core" % "jackson-core" % "2.8.3",
  "com.fasterxml.jackson.datatype" % "jackson-datatype-jsr310" % "2.8.6",
  "org.bitbucket.kiwicampus" % "epcommon_java" % "2.11",
  "com.typesafe.play.modules" %% "play-modules-redis" % "2.5.0",
  filters
)



